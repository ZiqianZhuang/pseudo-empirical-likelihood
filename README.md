# Overview

STAT854 project. Conduct a simulation to examine the pseudo-empirical likelihood estimation approach in estimating means of the finite population and building confidence interval under 3 special distributions. Different confidence interval generating methods were applied  to test the applicability of the approach.

# Simulation settings

(i).Non-symmetric continuous distribution.

(ii).Binary y.

(iii).Populations with many zeros.

The population size N=800 and the simulation runs B = 1000. We set the sampling rate as 5% and 10%,i.e. n = 40 and 80. 

This was implemented using R 3.6.0.

# Conclusion

Generally speaking, PEL has a good performance under above three scenarios whether based on asymptotic distribution or bootstrap method. In these scenarios, when we change values of correlation coefficients and other parameters, RB and MSE keep in a very low level, which shows the precision of the population mean estimation based on PEL. But the simulation results also show many issues. For example, the relatively low CP obtained by asymptotic distribution may remind us to modify the method of estimating design effect.




